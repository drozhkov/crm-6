const getDb = require('../db/utils').getDb;

async function getAllDepartments() {
  const db = await getDb();

  const [departments] = await db.get(`
    SELECT d.id, d.title, dc.ancestor_id AS ancestor, IF (_dc.descendant_id, 1, 0) AS hasDescendants
    FROM departments d
    LEFT JOIN departments_closure dc
    ON d.id = dc.descendant_id AND dc.depth = 1
    LEFT JOIN departments_closure _dc
    ON d.id = _dc.ancestor_id AND _dc.depth >= 1 GROUP BY d.id
  `);

  return departments;
}
exports.getAllDepartments = getAllDepartments;
