const Db = require('./db');

async function getDb() {
  return await Db.setup();
}

exports.getDb = getDb;