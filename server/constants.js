exports.PORT = 3002;
exports.SALT_ROUNDS = 10;

// Messages
exports.NO_SUCH_EMPLOYEE_MSG = 'There is no such employee. Please check email you entered.';
exports.WRONG_PASSWORD_MSG = "The email and password doesn't match.";
