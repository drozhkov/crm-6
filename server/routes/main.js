const express = require('express');
const getAllDepartments = require('../queries/departments').getAllDepartments;
const getAllEmployees = require('../queries/employees').getAllEmployees;
const prepareEmployeesData = require('../utils').prepareEmployeesData;
const prepareDepartmentsData = require('../utils').prepareDepartmentsData;

const employeesRouter = require('./employees').default;
const authorizationRouter = require('./authorization').default;

const router = express.Router();

router.get('/', async (req, res) => {
  res.redirect('/api');
});

router.get('/api', async (req, res, next) => {
  const departmentsRawData = await getAllDepartments();
  const employeesRawData = await getAllEmployees();
  const departments = prepareDepartmentsData(departmentsRawData);
  const employees = prepareEmployeesData(employeesRawData);

  res.json({ departments, employees });
});

router.use('/api/employees', employeesRouter);
router.use('/api/authorization', authorizationRouter);

module.exports = router;
