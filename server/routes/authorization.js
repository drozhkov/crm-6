const express = require('express');
const bcrypt = require('bcrypt');
const SALT_ROUNDS = require('../constants').SALT_ROUNDS;
const NO_SUCH_EMPLOYEE_MSG = require('../constants').NO_SUCH_EMPLOYEE_MSG;
const WRONG_PASSWORD_MSG = require('../constants').WRONG_PASSWORD_MSG;
const authorize = require('../queries/authorization').authorize;
const _PASSWORD = 'qwerty';

const router = express.Router();

router.post('/', async (req, res) => {
  const { email, password } = req.body;
  const employee = await authorize(email);

  if (employee) {
    (async () => {
      const hash = await bcrypt.hash(password, SALT_ROUNDS);
      const match = await bcrypt.compare(_PASSWORD, hash);
      if (match) {
        res.send({ id: match, token: '' });
      } else {
        res.send({
          error: {
            name: 'password',
            text: WRONG_PASSWORD_MSG
          }
        });
      }
    })();
  } else {
    res.send({
      error: {
        name: 'email',
        text: NO_SUCH_EMPLOYEE_MSG
      }
    });
  }
});

exports.default = router;
