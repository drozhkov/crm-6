import React from 'react';

export default function renderTree(departments, Component) {
  return departments.map(department => {
    if (department.children.length) {
      return (
        <li key={department.id}>
          <Component
            id={department.id}
            title={department.title}
            hasDescendants={department.hasDescendants}
          />
          <ul style={{ paddingLeft: '20px' }}>{renderTree(department.children, Component)}</ul>
        </li>
      );
    }
    return (
      <li key={department.id}>
        <Component id={department.id} title={department.title} />
      </li>
    );
  });
}
