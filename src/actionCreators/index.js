import axios from 'axios';
import {
  SORT_EMPLOYEE_LIST,
  FILTER_EMPLOYEE_LIST_BY_DEPARTMENT,
  FILTER_EMPLOYEE_LIST_BY_VACATION,
  SET_SEARCH_TERM,
  ID,
  NAME,
  SIGN_IN,
  SERVER_API_URL
} from '../constants';

function setEmployeeListFilter(filterBy) {
  return {
    type: filterBy.type,
    payload: filterBy.payload
  };
}

function getSorting(sortBy) {
  if (sortBy === ID) return NAME;
  return ID;
}

export function setEmployeeListSorting(sortBy) {
  return {
    type: SORT_EMPLOYEE_LIST,
    sortBy: getSorting(sortBy)
  };
}

export function setEmployeeListFilterByDepartment(filterBy) {
  return setEmployeeListFilter({
    type: FILTER_EMPLOYEE_LIST_BY_DEPARTMENT,
    payload: filterBy
  });
}

export function setEmployeeListFilterByVacation(filterBy) {
  return setEmployeeListFilter({
    type: FILTER_EMPLOYEE_LIST_BY_VACATION,
    payload: filterBy
  });
}

export function setSearchTerm(searchTerm) {
  return {
    type: SET_SEARCH_TERM,
    searchTerm
  };
}

export function signInAction(values, actions) {
  return dispatch => {
    axios({
      method: 'post',
      url: `${SERVER_API_URL}/authorization`,
      data: {
        email: values.email,
        password: values.password
      }
    }).then(res => {
      if (res.data.error) {
        actions.setFieldError(res.data.error.name, res.data.error.text);
      }
      if (res.data.id) {
        return dispatch({ type: SIGN_IN, payload: true });
      }
    });
  };
}
