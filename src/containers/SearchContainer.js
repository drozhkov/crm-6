import { connect } from 'react-redux';
import Search from '../components/Search/Search';
import { setSearchTerm } from '../actionCreators';

const mapStateToProps = state => ({
  searchTerm: state.searchTerm
});

const mapDispatchToProps = dispatch => ({
  setSearchTerm: searchTerm => dispatch(setSearchTerm(searchTerm))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
