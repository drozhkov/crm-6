import { connect } from 'react-redux';
import EmployeeList from '../components/EmployeeList/EmployeeList';

const mapStateToProps = state => ({
  data: state,
  sortBy: state.sortBy,
  filterByDepartment: state.filterByDepartment,
  filterByVacation: state.filterByVacation,
  searchTerm: state.searchTerm
});

export default connect(mapStateToProps)(EmployeeList);
