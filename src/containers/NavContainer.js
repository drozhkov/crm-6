import { connect } from 'react-redux';
import Nav from '../components/Nav/Nav';
import { SIGN_OUT } from '../constants';

const mapStateToProps = state => ({
  authorized: state.authorized
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch({ type: SIGN_OUT })
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Nav);
