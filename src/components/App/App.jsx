/* global localStorage */

import React from 'react';
import cx from 'classnames';
import style from './App.css';
import SearchContainer from '../../containers/SearchContainer';
import EmployeeListContainer from '../../containers/EmployeeListContainer';
import ControlContainer from '../../containers/ControlContainer';
import DepList from '../DepList/DepList';
import NavContainer from '../../containers/NavContainer';
import Spinner from '../Spinner/Spinner';
import { DIT_DATA } from '../../constants';
import { requestAPI, updateStateAfterRequest, writeDataToLS } from '../../utils/apiUtils';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false
    };
    this.data = null;
    this.stateUpdater = updateStateAfterRequest.bind(this);
    this.writeDataToLS = writeDataToLS.bind(this);
  }

  componentDidMount() {
    const cachedDITData = localStorage.getItem(DIT_DATA);

    if (cachedDITData) {
      this.data = JSON.parse(cachedDITData);
      this.setState({ dataLoaded: true });
    } else {
      requestAPI()([this.stateUpdater, this.writeDataToLS]);
    }
  }

  render() {
    const leftPanelClasses = cx(style.app__panel, style.app__panel_left);
    const rightPanelClasses = cx(style.app__panel, style.app__panel_right);

    const LeftComponent = this.state.dataLoaded ? (
      <DepList departments={this.data.departments} />
    ) : (
      <Spinner />
    );
    const RightComponent = this.state.dataLoaded ? (
      <EmployeeListContainer employees={this.data.employees} />
    ) : (
      <Spinner />
    );

    return (
      <div className={style.app}>
        <div className={leftPanelClasses}>
          <SearchContainer />
          <div className={style.app__wrapper}>{LeftComponent}</div>
        </div>
        <div className={rightPanelClasses}>
          <ControlContainer />
          <div className={style.app__wrapper}>{RightComponent}</div>
        </div>
        <NavContainer route="login">Login</NavContainer>
      </div>
    );
  }
}

export default App;
