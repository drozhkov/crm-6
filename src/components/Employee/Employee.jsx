/* global localStorage */

import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import style from './Employee.css';
import NavContainer from '../../containers/NavContainer';
import Spinner from '../Spinner/Spinner';
import { DIT_DATA } from '../../constants';
import { requestAPI, updateStateAfterRequest } from '../../utils/apiUtils';

class Employee extends React.Component {
  constructor(props) {
    super(props);
    this.id = Number(this.props.match.params.id);
    this.state = {
      dataLoaded: false
    };
    this.data = null;
    this.updateStateAfterRequest = updateStateAfterRequest.bind(this);
  }

  componentDidMount() {
    if (localStorage.getItem(DIT_DATA)) {
      const cacheData = JSON.parse(localStorage.getItem(DIT_DATA));
      [this.data] = cacheData.employees.filter(item => item.id === this.id);
      this.setState({ dataLoaded: true });
    } else {
      requestAPI(`/employees/?id=${this.id}`)([this.updateStateAfterRequest]);
    }
  }

  render() {
    if (this.state.dataLoaded) {
      const wrap13 = cx(style.employee__wrap, style.employee__wrap_1_3);
      const wrap23 = cx(style.employee__wrap, style.employee__wrap_2_3);
      const {
        id,
        firstName,
        secondName,
        position,
        birthday,
        departmentTitle,
        phone,
        email,
        vacationDate
      } = this.data;
      const name = `${firstName} ${secondName}`;

      return (
        <div className={style.employee}>
          <div>
            <h1 className={style.employee__title}>{name}</h1>
            <hr />
            <div className={style.employee__inner}>
              <div className={wrap13}>
                <img src="" alt={name} />
              </div>
              <div className={wrap23}>
                <table className={style.employee__table}>
                  <tbody>
                    <tr>
                      <td className={style.employee__info}>Id: </td>
                      <td>{id}</td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Position: </td>
                      <td>{position}</td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Department: </td>
                      <td>{departmentTitle}</td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Phone: </td>
                      <td>{phone}</td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Email: </td>
                      <td>
                        <a href={`mailto:${email}`}>{email}</a>
                      </td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Birthday: </td>
                      <td>{birthday}</td>
                    </tr>
                    <tr>
                      <td className={style.employee__info}>Vacation date: </td>
                      <td>{vacationDate}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <NavContainer route="/">Back</NavContainer>
          </div>
        </div>
      );
    }

    return (
      <div className={style.employee}>
        <Spinner />
        <NavContainer route="/">Back</NavContainer>
      </div>
    );
  }
}

Employee.propTypes = {
  match: PropTypes.shape({
    isExact: PropTypes.bool.isRequired,
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    }),
    path: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }).isRequired
};

export default Employee;
