import { SORT_EMPLOYEE_LIST } from '../constants';

export default function sortEmployeeList(state = 'id', action) {
  switch (action.type) {
    case SORT_EMPLOYEE_LIST:
      return action.sortBy;
    default:
      return state;
  }
}
